package org.example.cardgame3;

import java.util.List;
public class HandCheck {

  private final List<String> hand;

  public HandCheck (List<String> hand) {
    this.hand = hand;
  }


  private List<String> numbers(){
    return hand.stream().map(element -> element.substring(0, element.length()-1)).toList();

  }
  private List<String> suits(){
    return hand.stream().map(element -> element.substring(element.length()-1)).toList();
  }

  public int Sum() {
    return numbers().stream().mapToInt(Integer::parseInt).sum();
  }

  public boolean isFlush() {
    return suits().stream().allMatch(suit -> suit.equals(suits().getFirst()));
  }

  public String hearts() {
    List<String> listHearts = hand.stream().filter(hand -> hand.endsWith("H")).toList();
    return String.join(", ", listHearts);
  }
  public boolean QueenOfSpades() {
    return suits().stream().anyMatch(suit -> suit.equals("S")) && numbers().stream().anyMatch(number -> number.equals("Q"));
  }


}
