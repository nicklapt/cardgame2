package org.example.cardgame3;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
  private final List<String> deck;

  public DeckOfCards() {
    char[] suits = {'S', 'H', 'D', 'C'};
    String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "1"};
    deck = new ArrayList<>();
    for (char suit : suits) {
      for (String rank : ranks) {
        deck.add(rank + suit);
      }
    }
  }

  public List<String> getCards() {
    return deck;
  }

  public List<String> dealHand(int numCards) {
    List<String> hand = new ArrayList<>();
    for (int i = 0; i < numCards; i++) {
      Random random = new Random();
      int randomIndex = random.nextInt(deck.size());
      hand.add(deck.get(randomIndex));
    }
    return hand;
  }


}