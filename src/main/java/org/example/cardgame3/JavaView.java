package org.example.cardgame3;


import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import org.example.cardgame3.DeckOfCards;

public class JavaView extends Application {

  DeckOfCards deckOfCards = new DeckOfCards();
  private List<String> hand;


  public static void main(String[] args) {
    launch(args);
  }


  @Override
  public void start(Stage primaryStage) {

    hand = deckOfCards.dealHand(5);
    List<String> transformedHand = convertNumbersToJQKA(hand);
    HBox hBox = convertedHand(transformedHand);
    VBox vBox = checkedHandBox(hand);

    Pane pane = new HBox(10);


    Button button = new Button("Deal Hand");
    Button button2 = new Button("Check Hand");

    button.setOnAction(e -> {
      hand = deckOfCards.dealHand(5);
      List<String> transformedHandNew = convertNumbersToJQKA(hand);
      hBox.getChildren().clear();
      hBox.getChildren().add(convertedHand(transformedHandNew));
    });

    button2.setOnAction(e -> updateCheckedHandBox(vBox));

    VBox vBox2 = new VBox(10);
    vBox2.getChildren().addAll(hBox, button, button2, vBox, pane);

    Scene scene = new Scene(vBox2, 600, 350);
    primaryStage.setScene(scene);
    primaryStage.show();

  }

  private List<String> convertNumbersToJQKA(List<String> cards) {
    List<String> convertedCards = new ArrayList<>();
    cards.forEach(card -> {
      String rank = card.substring(0, card.length()-1);
      String suit = card.substring(card.length()-1);
      switch (rank) {
        case "11":
          convertedCards.add("J" + suit);
          break;
        case "12":
          convertedCards.add("Q" + suit);
          break;
        case "13":
          convertedCards.add("K" + suit);
          break;
        case "1":
          convertedCards.add("A" + suit);
          break;
        default:
          convertedCards.add(rank + suit);
      }
    });
    return convertedCards;
  }




  private StackPane card(String suit, String rank) {
    Rectangle rectangle = new Rectangle(100, 150);
    rectangle.setFill(Color.WHITE);
    rectangle.setStroke(Color.BLACK);
    Label label = new Label(rank);
    Label label2 = new Label(rank);
    Label label3 = new Label(suit);
    StackPane.setAlignment(label, Pos.TOP_LEFT);
    StackPane.setAlignment(label2, Pos.BOTTOM_RIGHT);
    StackPane.setAlignment(label3, Pos.CENTER);
    StackPane stackPane = new StackPane();
    stackPane.getChildren().addAll(rectangle, label, label2, label3);
    return stackPane;
  }

  private HBox convertedHand(List<String> cards) {
    HBox hBox = new HBox(5);
    for (String card : cards) {
      String rank = card.substring(0, card.length()-1);
      String suit = card.substring(card.length()-1);
      hBox.getChildren().add(card(suit, rank));
    }
    return hBox;
  }

  private VBox checkedHandBox(List<String> hand) {
    VBox vbox = new VBox(4);
    HandCheck handCheck = new HandCheck(hand);
    Label label = new Label("Sum: " + handCheck.Sum());
    Label label2 = new Label("Flush: " + handCheck.isFlush());
    Label label3 = new Label("Queen of Spades: " + handCheck.QueenOfSpades());
    Label label4 = new Label("Hearts: " + handCheck.hearts());
    vbox.getChildren().addAll(label, label2, label3, label4);
    return vbox;
  }

  private void updateCheckedHandBox(VBox vBox) {
    vBox.getChildren().clear();
    vBox.getChildren().addAll(checkedHandBox(hand));
  }

}
