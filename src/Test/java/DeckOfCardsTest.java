import static org.junit.jupiter.api.Assertions.assertEquals;

import org.example.cardgame3.DeckOfCards;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class DeckOfCardsTest {

  private static DeckOfCards deck;

  @BeforeAll
  static void setUp() {
    deck = new DeckOfCards();
  }

  @Nested
  @DisplayName("Positive tests")
  class PositiveTests {

    @Test
    @DisplayName("Test constructor makes right number of cards")
    void testConstructor() {
      assertEquals(52, deck.getCards().size());
    }

    @Test
    @DisplayName("Test cards is right joined")
    void testFirstCard() {

      assertEquals("2S", deck.getCards().get(0));
      assertEquals("3S", deck.getCards().get(1));
      assertEquals("2D", deck.getCards().get(26));
    }

    @Test
    @DisplayName("Test dealHand returns right number of cards")
    void testDealHand() {
      assertEquals(5, deck.dealHand(5).size());
    }

  }

}
