
import org.example.cardgame3.HandCheck;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class HandCheckTest {

  private static List<String> hand1;
  private static List<String> hand2;
  private static HandCheck handCheck;

  private static HandCheck handCheck2;

  @BeforeAll
  static void setUp() {
    hand1 = List.of("2S", "3S", "4S", "5S", "6S");
    hand2 = List.of("12S", "10S", "4H", "5S", "7H");
    handCheck = new HandCheck(hand1);
    handCheck2 = new HandCheck(hand2);
  }

  @Nested
  @DisplayName("Positive tests")
  class PositiveTests {

    @Test
    @DisplayName("Test sum of hand")
    void testSum() {
      assertEquals(20, handCheck.Sum());
      assertEquals(38, handCheck2.Sum());
    }

    @Test
    @DisplayName("Test isFlush")
    void testIsFlush() {
      assertTrue(handCheck.isFlush());
      assertFalse(handCheck2.isFlush());
    }

    @Test
    @DisplayName("Test hearts")
    void testHearts() {
      assertEquals("", handCheck.hearts());
      assertEquals("4H, 7H", handCheck2.hearts());
    }

    @Test
    @DisplayName("Test QueenOfSpades")
    void testQueenOfSpades() {
      assertFalse(handCheck.QueenOfSpades());
      assertTrue(handCheck2.QueenOfSpades());
    }

  }

}

